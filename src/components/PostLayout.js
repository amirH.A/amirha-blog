/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"

import "./core.css"

import PostTopNavBar from "./PostTopNavBar"
import Copyright from "./Copyright"
const PostLayout = ({ children }) => {
  return (
    <>
      <PostTopNavBar />
      <main>{children}</main>
      <Copyright />
    </>
  )
}

export default PostLayout
