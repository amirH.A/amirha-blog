import React from "react"

import "./style.css"
const TopNavBar = props => {
  return (
    <nav className="top-nav-bar">
      <ul>
        <li>
          <a href="#about">About</a>
        </li>
        <li>
          <a href="#projects">Projects</a>
        </li>
        <li>
          <a href="#contact">Contact</a>
        </li>
        <li>
          <a href="#blog">Blog</a>
        </li>
      </ul>
    </nav>
  )
}
export default TopNavBar
