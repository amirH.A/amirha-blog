import React from "react"
import { graphql, useStaticQuery, Link } from "gatsby"
import { FaRegCalendarAlt, FaTags } from "react-icons/fa"

import "./style.css"
import { formatDate } from "../../utils"

const PostEntry = ({ formattedDate, tags, title, excerpt, path }) => (
  <article className="post-entry">
    <div className="post-right-sidebar">
      <div className="post-publish-time">
        <FaRegCalendarAlt className="post-icon" />
        {formattedDate}
      </div>
      <div className="post-tag">
        <FaTags className="post-icon" />
        {tags}
      </div>
    </div>
    <div className="post-excerpt">
      <h2 className="post-title">{title}</h2>
      <p>{excerpt}</p>
      <Link to={path} className="secondary-blue">
        Read more
      </Link>
    </div>
  </article>
)
const ArticlesList = () => {
  const data = useStaticQuery(
    graphql`
      query AllBlogPosts {
        allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
          edges {
            node {
              excerpt(pruneLength: 280)
              frontmatter {
                title
                path
                date
                tags
              }
            }
          }
        }
      }
    `
  )

  return (
    <div className="articles-list">
      {console.log(data)}
      <div className="article-wrapper">
        {data.allMarkdownRemark.edges.map(post => {
          const { title, path, date, tags } = post.node.frontmatter
          const excerpt = post.node.excerpt
          return (
            <PostEntry
              formattedDate={formatDate(date)}
              title={title}
              path={path}
              tags={tags}
              excerpt={excerpt}
            />
          )
        })}
      </div>
    </div>
  )
}

export default ArticlesList
