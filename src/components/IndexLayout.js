/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"

import "./core.css"

import TopNavBar from "./TopNavBar"
import Copyright from "./Copyright"
const IndexLayout = ({ children }) => {
  return (
    <>
      <TopNavBar />
      <main>{children}</main>
      <Copyright />
    </>
  )
}

export default IndexLayout
