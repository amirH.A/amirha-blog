import React from "react"
import { Link } from "gatsby"

import { FaArrowLeft } from "react-icons/fa"

import "./style.css"
const PostTopNavBar = props => {
  return (
    <nav className="top-nav-bar">
      <ul>
        <li className="articles-back">
          <Link to="/">
            <div className="arrow-icon">
              <FaArrowLeft />
            </div>
            All Articles
          </Link>
        </li>
        <li>
          <a href="#about">About</a>
        </li>
        <li>
          <a href="#projects">Projects</a>
        </li>
        <li>
          <a href="#contact">Contact</a>
        </li>
        <li>
          <a href="#blog">Blog</a>
        </li>
      </ul>
    </nav>
  )
}
export default PostTopNavBar
