// dateString should be in the following format: YYYY-MM-DD
const formatDate = dateString => {
  const parsedDate = new Date(dateString)
  const monthShort = parsedDate.toLocaleString("en-GB", { month: "short" })
  const formattedDate = `${monthShort} ${parsedDate.getDate()}, ${parsedDate.getFullYear()}`
  return formattedDate
}

export { formatDate }
