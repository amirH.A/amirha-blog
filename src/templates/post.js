import React from "react"
import { graphql } from "gatsby"

import { FaRegCalendarAlt, FaTags } from "react-icons/fa"

import PostLayout from "../components/PostLayout"
import "./style.css"
import { formatDate } from "../utils"

const Template = ({ data }) => {
  const post = data.markdownRemark
  const { title, date, tags } = post.frontmatter

  return (
    <PostLayout>
      <div className="post-wrapper">
        <h1>{title}</h1>
        <div className="single-post-info-wrapper">
          <div className="single-post-publish-time">
            <FaRegCalendarAlt className="post-icon" />
            Published {formatDate(date)}
          </div>
          <div className="single-post-tag">
            <FaTags className="post-icon" />
            {tags}
          </div>
        </div>
        <article className="single-post-main">
          <div dangerouslySetInnerHTML={{ __html: post.html }} />
        </article>
      </div>
    </PostLayout>
  )
}

export default Template
export const postQuery = graphql`
  query BlogPost($path: String!) {
    markdownRemark(frontmatter: { path: { eq: $path } }) {
      frontmatter {
        date
        title
        path
        tags
      }
      html
    }
  }
`
