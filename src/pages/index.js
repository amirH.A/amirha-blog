import React from "react"
// import { Link } from "gatsby"

import "typeface-roboto"
import "typeface-roboto-mono"

import IndexLayout from "../components/IndexLayout"
import ArticlesList from "../components/ArticlesList"
// import Image from "../components/image"
// import SEO from "../components/seo"

const IndexPage = () => (
  <IndexLayout>
    {/* <SEO title="Home" /> */}
    <h1 className="blog-heading">
      Amirhossein Andohkosh - <span className="secondary-blue">Blog</span>
    </h1>
    <ArticlesList />
    {/* <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
      <Image />
    </div> */}
    {/* <Link to="/page-2/">Go to page 2</Link> */}
  </IndexLayout>
)

export default IndexPage
