---
path: '/second-post-t2'
date: '2020-02-07'
title: 'Second Blog'
author: 'Amirhossein Andohkosh'
description: 'This is my second blog of 2020!'
tags: DevOps
---

Here is my main content
It is very interesting.

Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea dignissimos
aut consequuntur aspernatur corrupti ratione,
<p align="center">
  <img width="460" height="300" src="https://images.pexels.com/photos/443446/pexels-photo-443446.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500">
</p>
blanditiis non? Odit.

Inline-style:

Reference-style:
![alt text][logo]

[logo]: https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 2"

Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia, recusandae fugit dolores doloribus unde tempore possimus explicabo atque culpa. Ea velit, ipsum doloremque similique tempore nemo odit praesentium! Consectetur, maxime.

Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia, recusandae fugit dolores doloribus unde tempore possimus explicabo atque culpa. Ea velit, ipsum doloremque similique tempore nemo odit praesentium! Consectetur, maxime.

Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia, recusandae fugit dolores doloribus unde tempore possimus explicabo atque culpa. Ea velit, ipsum doloremque similique tempore nemo odit praesentium! Consectetur, maxime.
