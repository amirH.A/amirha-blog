---
path: '/first-post'
date: '2020-03-08'
author: 'Amirhossein Andohkosh'
tags: News
title: "What's this blog about?"
description: 'Introducing my blog'
---

##Who I am

Hello there, I'm Amirhossein Andohkosh, a software engineer. I finally got
around to making my own blog! I wanted to create a custom blog for myself since 2011.
I've created a few different ones in the past some managed and some built
by myself but I never got around to creating one from the scratch. Back then
I was going to create a custom wordpress PHP template and create everything on
top off wordpress from scratch and did some experimentation but never got to
anything polished.

Fast-forward 9 years, I continued my path in learning about programming, and
since then I've graduated in computer science from the University of Cambridge
and now working as a software engineer.

The purpose of this blog is for me to write on technical topics, share new
and interesting things I've learned about and more importantly encourage myself
to do something every week that I'd be proud of writing about. This will also
give me the opurtuanity to become better at writing about technical topics and
get better at describing complex technical concepts to others.

##What you can read about

I'm always about learning about as many things as possible, at the end of the day
how can I have confidence in my solutions if I haven' compared it to anything else,
and I'm not up to date with what the industry standard is?

* Infrastructure
** Terrraform
** Security on AWS
** AWS vs GCP vs Azure
** Kuberentes on the thre
** EKS vs self manageed on AWS
* CI (My kuberentes executtor setup!)
* Security (+ cloud)
*
